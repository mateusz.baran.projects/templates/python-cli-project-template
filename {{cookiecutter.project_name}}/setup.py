from pathlib import Path

from setuptools import find_packages, setup

from {{cookiecutter.project_name}} import __version__

setup(
    name='{{cookiecutter.project_name}}',
    version=__version__,{% if cookiecutter.author_name != "" %}
    author='{{cookiecutter.author_name}}',{% endif %}{% if cookiecutter.author_email != "" %}
    author_email='{{cookiecutter.author_email}}',{% endif %}{% if cookiecutter.maintainer_name != "" %}
    maintainer='{{cookiecutter.maintainer_name}}',{% endif %}{% if cookiecutter.maintainer_email != "" %}
    maintainer_email='{{cookiecutter.maintainer_email}}',{% endif %}
    license='{{cookiecutter.license}}',{% if cookiecutter.url != "" %}
    url='{{cookiecutter.url}}',{% endif %}{% if cookiecutter.project_short_description != "" %}
    description='{{cookiecutter.project_short_description}}',{% endif %}
    long_description=Path('README.md').read_text(),
    long_description_content_type='text/markdown',
    packages=find_packages(),
    entry_points={'console_scripts': ['{{cookiecutter.project_name}} = {{cookiecutter.project_name}}.__main__:app']},
    python_requires='>={{cookiecutter.python}}',
    classifiers=[
        'Programming Language :: Python :: {{cookiecutter.python}}',{% if cookiecutter.license == "MIT License" %}
        'License :: OSI Approved :: MIT License',{% endif %}
    ],
    install_requires=[
        'typer',
    ],
    extras_require={
        'dev': [
            'bump2version',
            'pytest',
            'pytest-cov',
            'pytest-flake8',
            'pytest-isort',
            'pytest-mypy',
        ],
    },
)
