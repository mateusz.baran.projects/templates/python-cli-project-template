import logging

import typer

from {{cookiecutter.project_name}} import __version__


def logging_callback(
        level: str,
) -> None:
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger('{{cookiecutter.project_name}}')
    logger.addHandler(handler)
    logger.setLevel(level.upper())
    logger.propagate = False


def version_callback(show: bool) -> None:
    if show:
        typer.echo(f'{{cookiecutter.project_display_name}} version: {__version__}')
        raise typer.Exit()
