import typer

from {{cookiecutter.project_name}}.utils.callbacks import logging_callback, version_callback

app = typer.Typer(help="{{cookiecutter.project_short_description}}")


@app.callback(invoke_without_command=True)
def main(
        ctx: typer.Context,
        verbose: str = typer.Option(
            "error", "--verbose", callback=logging_callback, is_eager=True),
        version: bool = typer.Option(
            False, "--version", callback=version_callback, is_eager=True),
) -> None:
    if ctx.invoked_subcommand is None:
        typer.echo("Welcome to {{cookiecutter.project_display_name}}.")
        typer.echo("")
        typer.echo("Use --help option to see the guides.")


if __name__ == '__main__':
    app()
