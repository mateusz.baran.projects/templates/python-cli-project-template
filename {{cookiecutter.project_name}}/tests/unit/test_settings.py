import unittest
from pathlib import Path

from {{cookiecutter.project_name}} import settings


class TestSettings(unittest.TestCase):
    def test_project_dir__check_absolute_path(self):
        # GIVEN & WHEN & THEN
        self.assertIsInstance(settings.PROJECT_DIR, Path)
        self.assertTrue(settings.PROJECT_DIR.is_absolute())
