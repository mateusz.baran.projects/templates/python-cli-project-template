import logging
import unittest

import typer

from {{cookiecutter.project_name}}.utils import callbacks


class TestCallbacks(unittest.TestCase):
    def test_logging_callback__check_logger_configuration(self):
        # GIVEN & WHEN
        callbacks.logging_callback(level='info')

        # THEN
        logger = logging.getLogger('{{cookiecutter.project_name}}')
        self.assertEqual(logging.getLevelName('INFO'), logger.level)
        self.assertFalse(logger.propagate)

    def test_version_callback__check_show(self):
        # GIVEN & WHEN & THEN
        self.assertRaises(typer.Exit, callbacks.version_callback, show=True)
        self.assertIsNone(callbacks.version_callback(show=False))
