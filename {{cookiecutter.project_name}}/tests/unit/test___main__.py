import logging
import unittest

import typer
from typer.testing import CliRunner

from {{cookiecutter.project_name}} import __main__


class TestMain(unittest.TestCase):
    def setUp(self) -> None:
        self.runner = CliRunner()

    def test_main_app__check_verbose_option(self):
        # GIVEN & WHEN
        result = self.runner.invoke(__main__.app, ["--verbose", "info"])

        # THEN
        logger = logging.getLogger('{{cookiecutter.project_name}}')

        self.assertEqual(0, result.exit_code)
        self.assertIn("Welcome to", result.stdout)
        self.assertEqual(logging.getLevelName('INFO'), logger.level)

    def test_main_app__check_version_option(self):
        # GIVEN & WHEN
        result = self.runner.invoke(__main__.app, ["--version"])

        # THEN
        self.assertEqual(0, result.exit_code)
        self.assertIn("version", result.stdout)
        self.assertNotIn("Welcome to", result.stdout)

    def test_main_app__check_context(self):
        # GIVEN
        @__main__.app.command()
        def command():
            typer.echo("Hello world!")

        # WHEN
        result = self.runner.invoke(__main__.app, ["command"])

        # THEN
        self.assertEqual(0, result.exit_code)
        self.assertIn("Hello world!", result.stdout)
        self.assertNotIn("Welcome to", result.stdout)
