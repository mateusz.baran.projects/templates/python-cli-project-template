# {{cookiecutter.project_display_name}}
{{cookiecutter.project_short_description}}

## Requirements
Python {{cookiecutter.python}}+

## Installation
Clone the repository and then in the project folder run
```bash
pip install --editable .
```

or with development tools
```bash
pip install --editable .[dev]
```

## Project structure
    ├── storage
    ├── {{cookiecutter.project_name}}
    │    └── settings.py
    └── tests
    
## Example
{{cookiecutter.project_display_name}} can be used as CLI program
```bash
python {{cookiecutter.project_name}} --help
```

## Contributing
Contributions are very welcome. Tests can be run with
[tox](https://tox.readthedocs.io/en/latest/), please ensure the coverage
at least stays the same before you submit a merge request.

## License
{% if cookiecutter.license == "MIT License" %}This project is licensed under the terms of the [MIT](http://opensource.org/licenses/MIT) license.{% elif cookiecutter.license == "Not open source" %}This project is not open source.{% endif %}
{% if cookiecutter.maintainer_email != "" %}
## Issues
If you encounter any problems, please email us at <{{cookiecutter.maintainer_email}}>, along with a detailed description.
{% endif %}
