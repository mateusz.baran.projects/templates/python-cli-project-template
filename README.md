# Python CLI project template

The Python CLI cookiecutter project template. See also the report template at this [link](https://gitlab.com/mateusz.baran.projects/templates/machine-learning-report-template).

## Usage

First install cookiecutter
```bash
pip install cookiecutter
```

then clone template and follow for instructions
```bash
cookiecutter https://gitlab.com/mateusz.baran.projects/templates/python-cli-project-template
```

## Examples of project structure
### Machine Learning

    ├── storage
    │    ├── data
    │    │    ├── raw
    │    │    ├── samples
    │    │    ├── processed
    │    │    └── datasets
    │    │
    │    ├── configs
    │    ├── weights
    │    ├── logs
    │    └── reports
    │
    ├── project_name
    │    ├── data_processing
    │    │    ├── analysis
    │    │    ├── cleaning
    │    │    └── feature_extraction
    │    │
    │    ├── datasets
    │    ├── models
    │    ├── learning
    │    ├── predictors
    │    ├── experiments
    │    ├── utils
    │    ├── visualizations
    │    │    └── notebooks
    │    │         ├── 01-`notebook-name`.ipynb
    │    │         └── 02-`notebook-name`.ipynb
    │    │
    │    ├── __init__.py
    │    ├── __main__.py
    │    └── settings.py
    │
    ├── tests
    │    ├── unit
    │    │    └── test_`package_name`
    │    │         └── test_`module_name`.py
    │    │
    │    └── integration
    │         ├── fixtures
    │         └── test_`package_name`
    │              └── test_`module_name`.py
    │
    ├── .gitignore
    ├── .gitlab-ci.yml
    ├── LICENSE
    ├── README.md
    ├── setup.cfg
    └── setup.py


### Web Application

    ├── storage
    │    ├── configs
    │    └── logs
    │
    ├── project_name
    │    ├── core
    │    │    ├── routers
    │    │    └── api.py
    │    │
    │    ├── services
    │    │    ├── database
    │    │    └── worker
    │    │
    │    ├── models
    │    ├── utils
    │    │
    │    ├── __init__.py
    │    ├── __main__.py
    │    └── settings.py
    │
    ├── tests
    │    ├── unit
    │    │    └── test_`package_name`
    │    │         └── test_`module_name`.py
    │    │
    │    └── integration
    │         ├── fixtures
    │         └── test_`package_name`
    │              └── test_`module_name`.py
    │
    ├── .gitignore
    ├── .dockerignore
    ├── .gitlab-ci.yml
    ├── Dockerfile
    ├── LICENSE
    ├── README.md
    ├── setup.cfg
    └── setup.py

## License
This project is licensed under the terms of the [MIT](http://opensource.org/licenses/MIT) license.

## Issues
If you encounter any problems, please email us at <mateusz.baran.sanok@gmail.com>, along with a detailed description.
